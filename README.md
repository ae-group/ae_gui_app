<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# gui_app 0.3.106

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_gui_app/develop?logo=python)](
    https://gitlab.com/ae-group/ae_gui_app)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_gui_app/release0.3.104?logo=python)](
    https://gitlab.com/ae-group/ae_gui_app/-/tree/release0.3.104)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_gui_app)](
    https://pypi.org/project/ae-gui-app/#history)

>ae_gui_app package 0.3.106.

[![Coverage](https://ae-group.gitlab.io/ae_gui_app/coverage.svg)](
    https://ae-group.gitlab.io/ae_gui_app/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_gui_app/mypy.svg)](
    https://ae-group.gitlab.io/ae_gui_app/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_gui_app/pylint.svg)](
    https://ae-group.gitlab.io/ae_gui_app/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_gui_app)](
    https://gitlab.com/ae-group/ae_gui_app/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_gui_app)](
    https://gitlab.com/ae-group/ae_gui_app/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_gui_app)](
    https://gitlab.com/ae-group/ae_gui_app/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_gui_app)](
    https://pypi.org/project/ae-gui-app/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_gui_app)](
    https://gitlab.com/ae-group/ae_gui_app/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_gui_app)](
    https://libraries.io/pypi/ae-gui-app)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_gui_app)](
    https://pypi.org/project/ae-gui-app/#files)


## installation


execute the following command to install the
ae.gui_app package
in the currently active virtual environment:
 
```shell script
pip install ae-gui-app
```

if you want to contribute to this portion then first fork
[the ae_gui_app repository at GitLab](
https://gitlab.com/ae-group/ae_gui_app "ae.gui_app code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_gui_app):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_gui_app/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.gui_app.html
"ae_gui_app documentation").
